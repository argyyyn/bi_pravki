if (screen.width <= 780) {
	window.location.href = "mobile.html";
}
$('#fullpage').fullpage({
  sectionSelector: '.vertical-scrolling',
  navigation: true,
  anchors: ['s-1','s-2','s-3','s-4','s-5','s-6','s-7','s-8','s-9','s-10','s-11','s-12','s-13','s-14'],
  menu: '#menu',
  navigationTooltips: ["Главная", "О проекте", "Благоустройство", "Архитектор", "Дворовая территория", "Входная группа", "Фонтан", "Естественное освещение ", "Материалы", "Локация", "Значительная высота", "Окрестность Вашего Дома", "Свет и воздух", "Контакты"],
  normalScrollElements: '.modal-window, .overlay, .modalThreeLayout, .wait-back, .contact-block',
  keyboardScrolling: false,
  scrollingSpeed: 900,
  onLeave: function(anchorLink, index) {
      if (index === 3 || index ===4 || index === 6 || index === 7 || index === 8 || index === 10 || index === 11 || index === 13) {
          $('.header__toggle-menu').css("display", "inline-block");
          $('.kvartirs-click').css("display", "block");
          $('.header__phone').css("display", "none");
          $('.header__menu').css("display", "none");
          $('.social').css("display", "none");
          $('.phone-fixed').css("display", "flex");
          $('.logo2').css("display", "none");
          $('.myForm').css("display", "none");
          $('.myForm-block').css("display", "none");
          $('.myForm-logo').css("display", "none");
          $('.myForm').css("width", "60px")
          $('.myForm').css("padding", "15px")
          $('.myForm').css("width", "0")
          $('.myForm').css("padding", "0")
      } else if (index === 2) {
          $('.header__toggle-menu').css("display", "inline-block");
          $('.kvartirs-click').css("display", "block");
          $('.header__phone').css("display", "none");
          $('.header__menu').css("display", "none");
          $('.social').css("display", "none");
          $('.phone-fixed').css("display", "none");
          $('.logo2').css("display", "none");
          $('.myForm').css("display", "none");
          $('.myForm-block').css("display", "none");
          $('.myForm-logo').css("display", "none");
          $('.myForm').css("width", "60px")
          $('.myForm').css("padding", "15px")
          $('.myForm').css("width", "0")
          $('.myForm').css("padding", "0")
      } else if (index === 1) {
          $('.header__toggle-menu').css("display", "none");
          $('.header__phone').css("display", "block");
          $('.header__menu').css("display", "flex");
          $('.social').css("display", "none");
          $('.phone-fixed').css("display", "none");
          $('.logo2').css("display", "block");
          $('.myForm').css("display", "none");
          $('.myForm-block').css("display", "none");
          $('.myForm-logo').css("display", "none");
          $('.myForm').css("width", "60px")
          $('.myForm').css("padding", "15px")
          $('.myForm').css("width", "0")
          $('.myForm').css("padding", "0")
      } else if (index === 14) {
          $('.header__toggle-menu').css("display", "inline-block");
          $('.kvartirs-click').css("display", "block");
          $('.header__phone').css("display", "none");
          $('.header__menu').css("display", "none");
          $('.social').css("display", "block");
          $('.phone-fixed').css("display", "flex");
          $('.logo2').css("display", "none");
          $('.myForm').css("display", "none");
          $('.myForm-block').css("display", "none");
          $('.myForm-logo').css("display", "none");
          $('.myForm').css("width", "60px")
          $('.myForm').css("padding", "15px")
          $('.myForm').css("width", "0")
          $('.myForm').css("padding", "0")
      } else if(index === 9 || index === 12) {
          $('.header__toggle-menu').css("display", "inline-block");
          $('.kvartirs-click').css("display", "block");
          $('.header__phone').css("display", "none");
          $('.header__menu').css("display", "none");
          $('.social').css("display", "none");
          $('.phone-fixed').css("display", "none");
          $('.logo2').css("display", "none");
          $('.myForm').fadeIn();
          $('.myForm-block').css("display", "none");
          $('.myForm-logo').css("display", "block");
          setTimeout( function(){
              $('.myForm').css("width", "680px")
              $('.myForm').css("padding", "5px 0 5px 0")
              setTimeout( function(){
                  $('.myForm-block').fadeIn();
              },1000);
          },1000);
          setTimeout(() => {
              $('.myForm .close').click();
              $('.myForm').css("width", "0")
          }, 15000)
      }
  }
});

$('.section-three .content a').hover(function() {
  $('#picture, #title').removeClass().addClass(
    $(this).attr('rel'));
  $('.section-three .content a').removeClass('content__active');
  $(this).addClass('content__active');
});

$(".cancel").on({
    "mouseover" : function() {
       this.src = './assets/image/icon/cancel-hover.png';
     },
     "mouseout" : function() {
       this.src='./assets/image/icon/cancel.png';
     }
});
$(".next").on({
  "mouseover" : function() {
     this.src = './assets/image/icon/next-hover.png';
   },
   "mouseout" : function() {
     this.src='./assets/image/icon/next.png';
   }
});
$(".back").on({
  "mouseover" : function() {
     this.src = './assets/image/icon/back-hover.png';
   },
   "mouseout" : function() {
     this.src='./assets/image/icon/back.png';
   }
});

$('.myForm .close').click(function () {
    $('.myForm-block').css("display", "none");
    $('.myForm').css("width", "0")
    $('.myForm').css("padding", "0")
})
$('.myForm-logo').click(function () {
    $('.myForm').css("width", "820px")
    $('.myForm').css("padding", "5px 0 5px 0")
    setTimeout( function(){
        $('.myForm-block').fadeIn();
    },1000);
})

// Input Mask
$(function(){
  $.each($("[type='tel']"), function(i,v){
      $(this).inputmask({"mask": $(this).attr("mask"), "clearIncomplete": true});
  });
});

function openNav() {
  document.getElementById("myNav").style.height = "100%";
}
function closeNav() {
  document.getElementById("myNav").style.height = "0%";
}
function closeNav2() {
  setTimeout(function () {
    document.getElementById("myNav").style.height = "0%";
  }, 300);
}

ymaps.ready(init);

function init() {

  var myMap4 = new ymaps.Map("map4", {
    center: [43.207937, 76.893562],
    zoom: 15,
    controls: [],
    behaviors: ["rightMouseButtonMagnifier", "multiTouch"]
  })
  myMap4.geoObjects
  .add(new ymaps.Placemark([43.208096, 76.898436], {
      balloonContent: '<p class="text-white">Bi Group</p>'
  }, {
      preset: 'islands',
      iconColor: '#63BBE2'
  }))

  var myMap5 = new ymaps.Map("map5", {
    center: [51.150620, 71.447735],
    zoom: 12,
    controls: [],
    behaviors: ["rightMouseButtonMagnifier", "multiTouch"]
  })
  myMap5.geoObjects
  .add(new ymaps.Placemark([51.121977, 71.430402], {
      balloonContent: '<p class="text-white">Bi Group</p>'
  }, {
      preset: 'islands',
      iconColor: '#63BBE2'
  }))
  .add(new ymaps.Placemark([51.165771, 71.423290], {
    balloonContent: '<p class="text-white">Bi Group</p>'
  }, {
      preset: 'islands',
      iconColor: '#63BBE2'
  }))

  var myMap6 = new ymaps.Map("map6", {
    center: [47.108755, 51.914442],
    zoom: 14,
    controls: [],
    behaviors: ["rightMouseButtonMagnifier", "multiTouch"]
  })
  myMap6.geoObjects
  .add(new ymaps.Placemark([47.108369, 51.904806], {
      balloonContent: '<p class="text-white">Bi Group</p>'
  }, {
      preset: 'islands',
      iconColor: '#63BBE2'
  }))

  var myMap7 = new ymaps.Map("map7", {
    center: [42.335973, 69.625986],
    zoom: 13,
    controls: [],
    behaviors: ["rightMouseButtonMagnifier", "multiTouch"]
  })
  myMap7.geoObjects
  .add(new ymaps.Placemark([42.314814, 69.587756], {
      balloonContent: '<p class="text-white">Bi Group</p>'
  }, {
      preset: 'islands',
      iconColor: '#63BBE2'
  }))
  .add(new ymaps.Placemark([42.346042, 69.626370], {
      balloonContent: '<p class="text-white">Bi Group</p>'
  }, {
      preset: 'islands',
      iconColor: '#63BBE2'
  }))
}

$(".hover-one").hover(
  function () {
    // $('.subtitle-two,.subtitle-three,.subtitle-four').removeClass('subtitle-active');
    // $('.subtitle-one').addClass('subtitle-active');
    $('.hover-two,.hover-three,.hover-four').removeClass('active-color');
    $(this).addClass('active-color');
  }
);
$(".hover-two").hover(
  function () {
    // $('.subtitle-one,.subtitle-three,.subtitle-four').removeClass('subtitle-active');
    // $('.subtitle-two').addClass('subtitle-active');
    $('.hover-one,.hover-three,.hover-four').removeClass('active-color');
    $(this).addClass('active-color');
  }
);
$(".hover-three").hover(
  function () {
    // $('.subtitle-one,.subtitle-two,.subtitle-four').removeClass('subtitle-active');
    // $('.subtitle-three').addClass('subtitle-active');
    $('.hover-one,.hover-two,.hover-four').removeClass('active-color');
    $(this).addClass('active-color');
  }
);
$(".hover-four").hover(
  function () {
    // $('.subtitle-one,.subtitle-two,.subtitle-three').removeClass('subtitle-active');
    // $('.subtitle-four').addClass('subtitle-active');
    $('.hover-one,.hover-three,.hover-three').removeClass('active-color');
    $(this).addClass('active-color');
  }
);

var modal4 = document.getElementById('techOneModal');
var btn4 = document.getElementById("techOneBtn");
var exit4 = document.getElementById("close4");
var swipertechone;
var swipertechtwo;
var swipertechthree;
var swipertechfour;
var swipercomone;
var swipercomtwo;
var swipercomthree;
btn4.onclick = function() {
    modal4.style.opacity = 1;
    $(modal4).css("z-index", "102");
    swipertechone = new Swiper('.swiper-container-techone', {
        navigation: {
            nextEl: '.swiper-button-next-techone',
            prevEl: '.swiper-button-prev-techone',
        },
        initialSlide: 3
    });
}
exit4.onclick = function() {
    modal4.style.opacity = 0;
    $(modal4).css("z-index", "0");
    swipertechone.destroy();
}
var modal5 = document.getElementById('techTwoModal');
var btn5 = document.getElementById("techTwoBtn");
var exit5 = document.getElementById("close5");
btn5.onclick = function() {
    modal5.style.opacity = 1;
    $(modal5).css("z-index", "102");
    swipertechtwo = new Swiper('.swiper-container-techtwo', {
        navigation: {
            nextEl: '.swiper-button-next-techtwo',
            prevEl: '.swiper-button-prev-techtwo',
        },
        initialSlide: 1
    });
}
exit5.onclick = function() {
    modal5.style.opacity = 0;
    $(modal5).css("z-index", "0");
    swipertechtwo.destroy();
}

var modal7 = document.getElementById('techFourModal');
var btn7 = document.getElementById("techFourBtn");
var exit7 = document.getElementById("close7");
btn7.onclick = function() {
    modal7.style.opacity = 1;
    $(modal7).css("z-index", "102");
    swipertechfour = new Swiper('.swiper-container-techfour', {
        navigation: {
            nextEl: '.swiper-button-next-techfour',
            prevEl: '.swiper-button-prev-techfour',
        },
    });
}
exit7.onclick = function() {
    modal7.style.opacity = 0;
    $(modal7).css("z-index", "0");
    swipertechfour.destroy();
}
var modal8 = document.getElementById('comOneModal');
var btn8 = document.getElementById("comOneBtn");
var exit8 = document.getElementById("close8");
btn8.onclick = function() {
    modal8.style.opacity = 1;
    $(modal8).css("z-index", "102");
    swipercomone = new Swiper('.swiper-container-comone', {
        navigation: {
            nextEl: '.swiper-button-next-comone',
            prevEl: '.swiper-button-prev-comone',
        },
    });
}
exit8.onclick = function() {
    modal8.style.opacity = 0;
    $(modal8).css("z-index", "0");
    swipercomone.destroy();
}
var modal9 = document.getElementById('comTwoModal');
var btn9 = document.getElementById("comTwoBtn");
var exit9 = document.getElementById("close9");
btn9.onclick = function() {
    modal9.style.opacity = 1;
    $(modal9).css("z-index", "102");
    swipercomtwo = new Swiper('.swiper-container-comtwo', {
        navigation: {
            nextEl: '.swiper-button-next-comtwo',
            prevEl: '.swiper-button-prev-comtwo',
        },
        initialSlide: 2
    });
}
exit9.onclick = function() {
    modal9.style.opacity = 0;
    $(modal9).css("z-index", "0");
    swipercomtwo.destroy();
}
var modal10 = document.getElementById('comThreeModal');
var btn10 = document.getElementById("comThreeBtn");
var exit10 = document.getElementById("close10");
btn10.onclick = function() {
    modal10.style.opacity = 1;
    $(modal10).css("z-index", "102");
    swipercomthree = new Swiper('.swiper-container-comthree', {
        navigation: {
            nextEl: '.swiper-button-next-comthree',
            prevEl: '.swiper-button-prev-comthree',
        },
        initialSlide: 1
    });
}
exit10.onclick = function() {
    modal10.style.opacity = 0;
    $(modal10).css("z-index", "0");
    swipercomthree.destroy();
}

$(document).ready(function(){
  $('.btnShow').click(function(){
      $(this).parent().find(".item").fadeToggle();
  });
  $('.btnShow2').click(function(){
    $(this).parent().find(".item2").fadeToggle();
  });
  $('.btnShow3').click(function(){
    $(this).parent().find(".item3").fadeToggle();
  });
  $('.btnShow4').click(function(){
    $(this).parent().find(".item4").fadeToggle();
  });
});

$('#interview1').click(function(){
  $('.question').css("display", "none");
  $('.answer-one').fadeIn()
});
$('.close-interview1').click(function(){
  $('.question').fadeIn()
  $('.answer-one').css("display", "none");
});
$('#interview2').click(function(){
  $('.question').css("display", "none");
  $('.answer-two').fadeIn()
});
$('.close-interview2').click(function(){
  $('.question').fadeIn()
  $('.answer-two').css("display", "none");
});
$('#interview3').click(function(){
  $('.question').css("display", "none");
  $('.answer-three').fadeIn()
});
$('.close-interview3').click(function(){
  $('.question').fadeIn()
  $('.answer-three').css("display", "none");
});
$('#interview4').click(function(){
  $('.question').css("display", "none");
  $('.answer-four').fadeIn()
});
$('.close-interview4').click(function(){
  $('.question').fadeIn()
  $('.answer-four').css("display", "none");
});
$('#interview5').click(function(){
  $('.question').css("display", "none");
  $('.answer-five').fadeIn()
});
$('.close-interview5').click(function(){
  $('.question').fadeIn()
  $('.answer-five').css("display", "none");
});



var lFollowX = 0,
    lFollowY = 0,
    x = 0,
    y = 0,
    friction = 1 / 30;
function moveBackground() {
    x += (lFollowX - x) * friction;
    y += (lFollowY - y) * friction;

    //  translate = 'translateX(' + x + 'px, ' + y + 'px)';
    translate = 'translateX(' + x + 'px) translateY(' + y +'px)';
    translate5 = 'translateX(' + x * 3 + 'px) translateY(' + y * 3 +'px)';
    $('.animate-this').css({
        '-webit-transform': translate,
        '-moz-transform': translate,
        'transform': translate
    });
    $('.animate-this5').css({
        '-webit-transform': translate5,
        '-moz-transform': translate5,
        'transform': translate5
    });
    window.requestAnimationFrame(moveBackground);
}

$(window).on('mousemove click', function(e) {
    var isHovered = $('.animate-this:hover').length > 0;
    //if(!$(e.target).hasClass('animate-this')) {
    if(!isHovered) {
        var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX)),
            lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));

        lFollowX = (20 * lMouseX) / 100;
        lFollowY = (10 * lMouseY) / 100;
    }
});
moveBackground();


$("form").submit(function() { //Change
    var th = $(this);
    $.ajax({
        type: "POST",
        url: "/mail.php", //Change
        data: th.serialize()
    }).done(function() {
        alert("Спасибо!");
        location.href= "/#"
        setTimeout(function() {
            // Done Functions
            th.trigger("reset");
        }, 1000);
    });
    return false;
});

function Parallax(options){
    options = options || {};
    this.nameSpaces = {
        wrapper: options.wrapper || '.parallax',
        layers: options.layers || '.parallax-layer',
        deep: options.deep || 'data-parallax-deep'
    };
    this.init = function() {
        var self = this,
            parallaxWrappers = document.querySelectorAll(this.nameSpaces.wrapper);
        for(var i = 0; i < parallaxWrappers.length; i++){
            (function(i){
                parallaxWrappers[i].addEventListener('mousemove', function(e){
                    var x = e.clientX,
                        y = e.clientY,
                        layers = parallaxWrappers[i].querySelectorAll(self.nameSpaces.layers);
                    for(var j = 0; j < layers.length; j++){
                        (function(j){
                            var deep = layers[j].getAttribute(self.nameSpaces.deep),
                                disallow = layers[j].getAttribute('data-parallax-disallow'),
                                itemX = (disallow && disallow === 'x') ? 0 : x / deep,
                                itemY = (disallow && disallow === 'y') ? 0 : y / deep;
                            if(disallow && disallow === 'both') return;
                            layers[j].style.transform = 'translateX(' + itemX + '%) translateY(' + itemY + '%)';
                        })(j);
                    }
                })
            })(i);
        }
    };
    this.init();
    return this;
}

window.addEventListener('load', function(){
    new Parallax();
});


//socials
let items = document.querySelectorAll(".socials-item-icon"),
    self = this;
items.forEach((item, index) => {
    item.addEventListener("mousemove", mouseMove);
    item.addEventListener("mouseleave", mouseLeave);
});

function mouseMove(e) {
    let target = e.target.closest("a"),
        targetData = target.getBoundingClientRect(),
        targetIcon = target.querySelector("i"),
        offset = {
            x: ((e.pageX - (targetData.left + targetData.width / 2)) / 4) * -1,
            y: ((e.pageY - (targetData.top + targetData.height / 2)) / 4) * -1
        };
    target.style.transform = "translate(" + offset.x + "px," + offset.y + "px) scale(" + 1.1 + ")";
    target.style.webkitTransform = "translate(" + offset.x + "px," + offset.y + "px) scale(" + 1.1 + ")";
    document.querySelectorAll(".socials-item-icon").forEach((e) => {
        if (e !== target) {
            e.style.transform = "translate(" + offset.x / 2 + "px, " + offset.y / 2 + "px) scale(" + 0.9 + ")";
            e.style.webkitTransform = "translate(" + offset.x / 2 + "px, " + offset.y / 2 + "px) scale(" + 0.9 + ")";
        }
    });
    targetIcon.style.transform = "translate(" + offset.x + "px," + offset.y + "px) scale(" + 1.1 + ")";
    targetIcon.style.webkitTransform = "translate(" + offset.x + "px," + offset.y + "px) scale(" + 1.1 + ")";
}

function mouseLeave(e) {
    document.querySelectorAll(".socials-item-icon").forEach((target) => {
        let targetIcon = target.querySelector("i");
        target.style.transform = "translate(0px,0px) scale(1)";
        target.style.webkitTransform = "translate(0px,0px) scale(1)";
        targetIcon.style.transform = "translate(0px,0px) scale(1)";
        targetIcon.style.webkitTransform = "translate(0px,0px) scale(1)";
    });
}


function generateWind (speed = 1) {
    var myDiv = document.getElementById('argo')

    var c = document.createElement('canvas')
    var $ = c.getContext('2d')

    var w = (c.width = window.innerWidth)
    var h = (c.height = window.innerHeight/2)



    var draw = function (a, b, t) {
        $.lineWidth = 0.2
        $.fillStyle = '#4F0020'
        $.fillRect(0, 0, w, h)
        for (var i = -60; i < 60; i += 1) {
            $.strokeStyle = 'white'
            $.beginPath()
            $.moveTo(0, h / 1.5)
            for (var j = 0; j < w; j += 10) {
                $.lineTo(
                    10 * Math.sin(i / 10) + j + 0.012 * j * j,
                    Math.floor(
                        h / 2 +
                        (j / 2) * Math.sin(j / 50 - t / 50 - i / 118) +
                        i * 0.9 * Math.sin(j / 25 - (i + t) / 65)
                    )
                )
            }
            $.stroke()
        }
    }
    var t = 0

    window.addEventListener(
        'resize',
        function () {
            c.width = w = window.innerWidth
            c.height = h = window.innerHeight
            $.fillStyle = '#4F0020'
        },
        false
    )

    var run = function () {
        window.requestAnimationFrame(run)
        t += speed
        draw(23, 32 * Math.sin(t / 2400), t)
    }
    myDiv.appendChild(c)
    run()

}
generateWind();

$('.interview-next1').click(function(){
    $('.question').css("display", "none");
    $('.answer').css("display", "none");
    $('.answer-two').fadeIn()
});
$('.interview-next2').click(function(){
    $('.question').css("display", "none");
    $('.answer').css("display", "none");
    $('.answer-three').fadeIn()
});
$('.interview-next3').click(function(){
    $('.question').css("display", "none");
    $('.answer').css("display", "none");
    $('.answer-four').fadeIn()
});
$('.interview-next4').click(function(){
    $('.question').css("display", "none");
    $('.answer').css("display", "none");
    $('.answer-five').fadeIn()
});
$('.interview-next5').click(function(){
    $('.question').css("display", "none");
    $('.answer').css("display", "none");
    $('.answer-one').fadeIn()
});
